import javax.swing.*;
import java.awt.*;

public class Main {
    public static JFrame fenetre;
    public static boolean run = true;
    public static final GraphicsDevice DEVICE = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

    public static GameState state = GameState.home;

    private static HomePage home = new HomePage();

    private static int ticksPerSecond;
    private static int skip;
    private static int max;
    private static double next_game_tick;

    public static void main(String[] args) {
            fenetre = new JFrame();
            Image icon = Toolkit.getDefaultToolkit().getImage("src/logo.png");

            fenetre.setBounds(5, 5, 500, 500);
            fenetre.setTitle("A prendre ou a laisser");
            fenetre.setIconImage(icon);
            fenetre.setResizable(false);
            fenetre.setUndecorated(true);
            fenetre.setVisible(true);
            fenetre.setAlwaysOnTop(true);
            fenetre.add(home);
            fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            DEVICE.setFullScreenWindow(fenetre);

            setFramerate(60);
            int loops;
            while(run)
            {
                loops = 0;
                while (System.currentTimeMillis() > next_game_tick && loops < max)
                {

                    if(state == GameState.home) {
                        home.repaint();
                    }

                    next_game_tick += skip;
                    loops++;
                }
            }

            fenetre.dispose();
    }

    private static void setFramerate(int tps)
    {
        ticksPerSecond = tps;
        skip = 1000 / ticksPerSecond;
        max = 5;
        next_game_tick = System.currentTimeMillis();
    }

}
