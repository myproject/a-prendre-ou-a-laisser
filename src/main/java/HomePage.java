import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class HomePage extends JPanel implements KeyListener, MouseListener {

    public HomePage()
    {
        addMouseListener(this);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }


    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, Main.fenetre.getWidth(), Main.fenetre.getHeight());
        Image logo = Toolkit.getDefaultToolkit().getImage("src/logo.png");
        g.drawImage(logo, Main.fenetre.getWidth() / 2 - 150, Main.fenetre.getHeight() / 2 - 500, null);
        g.setColor(Color.WHITE);
        g.fillRoundRect(Main.fenetre.getWidth() / 2 - 200, Main.fenetre.getHeight() - 500, 400, 100, 50, 50);
        Point position = Main.fenetre.getMousePosition();
        if(position != null) {
            if(position.x > Main.fenetre.getWidth() / 2 - 200 && position.x < Main.fenetre.getWidth() / 2 + 200 && position.y > Main.fenetre.getHeight() - 500 && position.y < Main.fenetre.getHeight() - 400) {
                g.setColor(Color.RED);
                g.fillRoundRect(Main.fenetre.getWidth() / 2 - 200, Main.fenetre.getHeight() - 500, 400, 100, 50, 50);
            }
        }
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.BOLD, 30));
        g.drawString("Press space to start", Main.fenetre.getWidth() / 2 - 150, Main.fenetre.getHeight() - 450);

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            Main.run = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        Point position = Main.fenetre.getMousePosition();
        if(e.getButton() == MouseEvent.BUTTON1)
        {
            if(position != null) {
                if(position.x > Main.fenetre.getWidth() / 2 - 200 && position.x < Main.fenetre.getWidth() / 2 + 200 && position.y > Main.fenetre.getHeight() - 500 && position.y < Main.fenetre.getHeight() - 400) {
                    //TODO: Start the game
                    Main.state = GameState.game;
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }
}
